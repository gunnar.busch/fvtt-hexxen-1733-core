/**
 * Implementation of the german RPG HeXXen 1733 (c) under the license of https://ulissesspiele.zendesk.com/hc/de/articles/360017969212-Inhaltsrichtlinien-f%C3%BCr-HeXXen-1733-Scriptorium.
 * Implementation based on the content of http://hexxen1733-regelwiki.de/
 * Author: Martin Brunninger
 * Software License: GNU GPLv3
 */

class HexxenActorSheet extends HexxenSheetMixin(ActorSheet) {

  /** @override */
	async _renderOuter(options) {
    const outer = await super._renderOuter(options);
    // FIXME: deaktiviert bis fertige Grafiken
    // if (this.actor.type === 'character' && this.actor.isOwner) { 
    //   const html = $(outer);
    //   html.prepend($('<div class="deco-header"><div class="header-decoration"></div></div>'));
    //   html.find('.deco-header').append(html.find('.window-header').detach());
    // }
    // TODO: andere Decorations
    return outer;
  }

  /** @override */
  _getHeaderButtons() {
    let buttons = super._getHeaderButtons();

    // Token Configuration
    let canConfigure = this.options.editable && (game.user.isGM || this.actor.isOwner);
    if (canConfigure) {
      buttons = [
        {
          label: this.editMode ? "To Game Mode" : "To Edit Mode",
          class: "configure-edit",
          icon: `fas fa-${this.editMode ? "dice" : "edit"}`,
          onclick: ev => this._onToggleEditMode(ev)
        }
      ].concat(buttons);
    }
    return buttons
  }

  _onToggleEditMode(event) {
    event.preventDefault();

    let mode = this.editMode;
    // toggle mode
    mode = !mode;

    // save changed flag (also updates inner part of actor sheet)
    this.actor.setFlag(Hexxen.scope, "editMode", mode);

    // update button
    // FIXME: was passiert remote?
    // TODO: besser im activateListener oder _replaceHTML machen
    event.target.childNodes.forEach((node) => {
      if(node.className){
        node.className = "fas fa-" + (mode ? "dice" : "edit");
      } else if(node.textContent.match("Mode")){
        node.textContent = mode ? "To Game Mode" : "To Edit Mode";
      }
    });
  }

  get editMode() {
    return !! this.actor.getFlag(Hexxen.scope, "editMode");
  }

  /** @override */
  async getData() {
    const out = await super.getData();
    out.editMode = this.editMode;
    return out;
  }
}

