﻿/**
 * Implementation of the german RPG HeXXen 1733 (c) under the license of https://ulissesspiele.zendesk.com/hc/de/articles/360017969212-Inhaltsrichtlinien-f%C3%BCr-HeXXen-1733-Scriptorium.
 * Implementation based on the content of http://hexxen1733-regelwiki.de/
 * Author: Martin Brunninger
 * Software License: GNU GPLv3
 */

/**
 * HeXXen Roller Application
 * @type {FormApplication}
 * @param entity {Entity}      The Entity object for which the sheet is being configured
 * @param options {Object}     Additional Application options
 */
class HexxenRoller extends FormApplication {

  constructor(entity=null, options={}, hints={}) {
    super(entity, options);
    this.hints = hints;
    if (!hints.key && !hints.formula) {
      this.options.closeOnSubmit = false;
    }
  }

	static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["hexxen", "roller"],
      id: "roller",
      template: `${Hexxen.basepath}/templates/roller.html`,
      width: 300,
    });
  }

  /* -------------------------------------------- */

  /** @override */
  get id() {
    return "roller-" + this.appId;
  }

  /**
   * Add the Entity name into the window title
   * @type {String}
   */
  get title() {
    return "HeXXen Würfel Tool";
  }

  /* -------------------------------------------- */

  /**
   * Construct and return the data object used to render the HTML template for this form application.
   * @return {Object}
   */
  getData() {
    const out = super.getData() // object == entity, options == options
    const actorData = out.object?.system;
    out.hints = this.hints;

    let type = this.hints.type;
    let key = this.hints.key;
    let modifier = Number(this.hints.modifier) || 0;
    out.manual = key ? false : true;

    let result = {};
    out.data = result;
    let dice = { 
      "h": { label: "Hexxen", count: 0 },
      "g": { label: "GM", count: 0 },
      "+": { label: "Bonus", count: 0 },
      "-": { label: "Malus", count: 0 },
      "s": { label: "Segnung", count: 0 },
      "b": { label: "Blut", count: 0 },
      "e": { label: "Elixir", count: 0 },
      "f": { label: "Fluch", count: 0 }
    };
    if (!game.user.isGM) {
      delete dice['g'];
      delete dice['f'];
    }
    result.dice = dice;

    result.type = type;
    result.key = key;
    result.label = key;
    result.modifier = 0;
    result.value = 0;


    if ('formula' === type) {
      result.label = this.hints.flavour;
      const formula = this.hints.formula;
      if (formula.match(HexxenRoll.HEXXEN_MATCH_REGEX)) {
        let parts = formula.match(HexxenRoll.HEXXEN_SPLIT_REGEX);
        parts.forEach(p => {
          const [m, count=1, dice] = p.match(/(\d*)(.)/);
          // treat GM dice as Hexxen dice
          result.dice[dice].count += Number(count);
        })
      }
      else {
        ui.notifications.warn('unbekannte Formel');
        throw 'unbekannte Formel';
      }
    }
    else if ("attribute" === type) {
      let attribute = actorData.attributes[key];
      result.value = attribute.value;
      result.dice.h.count = attribute.value;
      result.label = attribute.label;
    }
    else if ("skill" === type) {
      let skill = actorData.skills[key] ?? actorData.skills[HexxenActor.renamedSkill(key)];
      if (!skill) {
        ui.notifications.warn('unbekannte Fertigkeit');
        throw 'unbekannte Fertigkeit';
      }
      let attribute = actorData.attributes[skill.attribute];
      let rolls = Number(skill.value) + Number(attribute.value);
      result.value = rolls;
      result.dice.h.count = rolls;
      result.label = skill.label;
    }
    else if ("combat" === type) {
      let combat = actorData.combat[key];
      if (!combat) {
        ui.notifications.warn('unbekannte Fertigkeit');
        throw 'unbekannte Fertigkeit';
      }
      let attribute = actorData.attributes[combat.attribute];
      let rolls = Number(combat.value) + Number(attribute.value);
      result.value = rolls;
      result.dice.h.count = rolls;
      result.label = combat.label;
      if (combat.schaden) result.label += ` (SCH +${combat.schaden})`; // FIXME: überschneidet sich mit modifier
    }

    if (modifier < 0) {
      result.label += ` - ${Math.abs(modifier)}`;
      result.dice['-'].count -= modifier; // require positive count
    }
    else if (modifier > 0) {
      result.label += ` + ${modifier}`;
      result.dice['+'].count += modifier;
    }

    return out;
  }

  /* -------------------------------------------- */

  /** @override */
  activateListeners(html) {
    super.activateListeners(html);

    html.find(".dice").on("click", ".control", this._onClickPlusMinus.bind(this));
  }

  async _onClickPlusMinus(event) {
    event.preventDefault();

    const a = event.currentTarget;
    const action = a.dataset.action;
    const inc = "increase" === action ? 1 : -1;
    const target = a.parentNode.dataset.key;
    const form = this.form;

    let e = form.elements["dice." + target];
    e.value = Number(e.value) + inc;
  }

  /**
   * This method is called upon form submission after form data is validated
   * @param event {Event}       The initial triggering submission event
   * @param formData {Object}   The object of validated form data with which to update the object
   * @private
   */
  async _updateObject(event, formData) {
    event.preventDefault();

    const roll = {};
    for ( let key of Object.keys(formData) ) {
      if ( key.startsWith("dice.") ) {
        const die = key.substr(5);
        // TODO: Workaround für negative Zähler
        const count = Math.abs(formData[key]);
        roll[die] = count;
      }
    }
    HexxenRollHelper.rollToChat(this.object, roll, formData.comment);
  }

  roll() {
    const data = this.getData();
    const roll = {};
    for ( let die of Object.keys(data.data.dice) ) {
      // TODO: Workaround für negative Zähler
      const count = Math.abs(data.data.dice[die].count);
      roll[die] = count;
    }
    HexxenRollHelper.rollToChat(this.object, roll, data.data.label);
  }

}
