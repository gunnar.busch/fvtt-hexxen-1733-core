/**
 * Implementation of the german RPG HeXXen 1733 (c) under the license of https://ulissesspiele.zendesk.com/hc/de/articles/360017969212-Inhaltsrichtlinien-f%C3%BCr-HeXXen-1733-Scriptorium.
 * Implementation based on the content of http://hexxen1733-regelwiki.de/
 * Author: Martin Brunninger
 * Software License: GNU GPLv3
 */

const HexxenSheetMixin = Base => class extends Base {

  /** @override */
  setPosition(options={}) {
    // IMPORTANT: when used with Popout-Addon, position might not contain the correct dimensions!
    const position = super.setPosition(options);
    // if there is a scrollable sheet body (class 'sheet-body'), calc it's height
    HexxenDOMHelper.calcSheetBodyHeight(this.element);
    return position;
  }

  async _enrichHTML(value) {
    return await TextEditor.enrichHTML(value, { async: true });
  }

}
