﻿/**
 * Implementation of the german RPG HeXXen 1733 (c) under the license of https://ulissesspiele.zendesk.com/hc/de/articles/360017969212-Inhaltsrichtlinien-f%C3%BCr-HeXXen-1733-Scriptorium.
 * Implementation based on the content of http://hexxen1733-regelwiki.de/
 * Author: Martin Brunninger
 * Software License: GNU GPLv3
 */

class JaegerSheet extends HexxenActorSheet {

  /** @override */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["hexxen", "sheet", "actor", "jaeger", game.settings.get(Hexxen.scope, Hexxen.COLOR_THEME_SETTING_KEY)],
      template: `${Hexxen.basepath}/templates/jaeger-sheet.html`,
      width: 700,
      height: 720,
      tabs: [{navSelector: ".sheet-tabs", contentSelector: ".sheet-body", initial: "skills"}],
      scrollY: [ ".biography.scroll-y", ".states.scroll-y", ".skills.scroll-y", ".powers.scroll-y",
        ".combat.scroll-y", ".items.scroll-y" ],
      dragDrop: [
        {dragSelector: ".items-list .item", dropSelector: null},
        {dragSelector: ".roll", dropSelector: null}
      ]
    });
  }

  /* -------------------------------------------- */

  /** @override */
  get title() {
    return `${super.title} (${this.actor.isHunter2e ? '2E' : '1E'}, Lv. ${this.actor.level})`;
  }

  /* -------------------------------------------- */

  /** @override */
  async getData() {
    // get duplicated data
    const out = await super.getData();
    const actorData = out.actor.system;

    // contains (from BaseEntitySheet)
    //   entity: any; (copy of this.actor.data; data only, not the Actor instance)
    //   owner: boolean;
    //   limited: boolean;
    //   options: any;
    //   editable: boolean; (not read-only)
    //   cssClass: string;
    // (from ActorSheet)
    //   actor: any; (alias for entity)
    //   data: any; (alias for actor.data; inner data)
    //   items: any; (alias for actor.items; data only, not the Item instance; sorted, contains all subtypes)
    // (from HexxenActorSheet)
    //   editMode: boolean; (full set (true) or limited set (false) of form elements)

    // TODO: verallgemeinern (alle Items) Achtung: so wird das echte Item Objekt modifiziert!
    // out.actor.items.forEach(i => {
      // i.data.description = i.data.summary ? i.data.summary : i.data.description;
      // if (i.type === 'item' && i.data.data.quantity === null) i.data.data.quantity = 0; // TODO: gehört ins Item Dokument
    // });

    // header resources
    let hres = {}
    for( let key of [ "ideen", "coups", "segnungen", "rage", "ambition", "quintessenz" ] ) {
      // TODO: temporärer Code bis zur Änderung der Datenstruktur im Actor
      hres[key] = {value: actorData.resources[key]};
      // hres[key] = data.data.resources[key];
    }
    // TODO: temporärer Code bis zur Änderung der Datenstruktur im Actor
    hres["ideen"].label = "Ideen";
    hres["ideen"].default = actorData.attributes.WIS.value + actorData.temp["idee-bonus"];
    hres["coups"].label = "Coups";
    hres["coups"].default = actorData.attributes.ATH.value + actorData.temp["coup-bonus"];
    hres["segnungen"].label = "Segnungen";
    hres["segnungen"].max = 5;
    hres["rage"].label = "Rage";
    hres["rage"].max = 5;
    hres["ambition"].label = "Ambitionen";
    hres["ambition"].max = 5;
    hres["quintessenz"].label = "Quintessenz";
    hres["quintessenz"].max = 5;
    out["header-resources"] = hres;

    // TODO: hints auf localize umstellen
    out.data.system.motivation["available-hint"] = "Keine Motivation ausgewählt";
    out.data.system.motivation["hint"] = "Verfügbar ab Jägerstufe 1";
    out.data.system.motivation.type = "Motivation";
    // FIXME: das sollte (teilweise?) bereits im actor.prepare() passieren
    let mot = this.actor.itemTypes.motivation; // returns items, not data
    mot = mot.length > 0 ? mot[0] : undefined;
    if (mot) {
      out.data.system.motivation.item = mot;
      out.data.system.motivation.bonus = mot.summaryOrDescription;
    }

    // TODO: hints auf localize umstellen
    out.data.system["role-1"]["available-hint"] = "Keine Rolle ausgewählt";
    out.data.system["role-1"]["hint"] = "Verfügbar ab Jägerstufe 1";
    out.data.system["role-1"].type = "Rolle";
    out.data.system["role-2"]["available-hint"] = "Keine Rolle ausgewählt";
    out.data.system["role-2"]["hint"] = "Verfügbar ab Jägerstufe 2";
    out.data.system["role-2"].type = "Rolle";
    out.data.system["role-3"]["available-hint"] = "Keine Rolle ausgewählt";
    out.data.system["role-3"]["hint"] = "Verfügbar ab Jägerstufe 7";
    out.data.system["role-3"].type = "Rolle";
    // FIXME: das sollte (teilweise?) bereits im actor.prepare() passieren
    let role = this.actor.itemTypes.role; // returns items, not data
    switch (role.length) {
      case 3:
        out.data.system["role-3"].item = role[2];
        // no break
      case 2:
        out.data.system["role-2"].item = role[1];
        // no break
      case 1:
        out.data.system["role-1"].item = role[0];
        // no break
      default:
    }

    // TODO: hints auf localize umstellen
    out.data.system.profession["available-hint"] = "Keine Profession ausgewählt";
    out.data.system.profession["hint"] = "Verfügbar ab Jägerstufe 2";
    out.data.system.profession.type = "Profession";
    // FIXME: das sollte (teilweise?) bereits im actor.prepare() passieren
    let prof = this.actor.itemTypes.profession;
    prof = prof.length > 0 ? prof[0] : undefined;
    if (prof) {
      out.data.system.profession.item = prof;
    }

    // FIXME: temporary code until array annotation is identified
    const languages = {};
    languages.value = out.data.system.languages["0"].value;
    out.data.system.languages = languages;

    out.stypes = { "idmg": "Innerer Schaden", "odmg": "Äußerer Schaden", "mdmg": "Malusschaden", "ldmg": "Lähmungsschaden" };
    for ( let state of Object.values(out.data.system.states) ) {
      state.type = out.stypes[state.type];
    }

    // TODO: gehört teilweise in den Jaeger!
    // Skills aufbereiten
    out.data.system.skills = out.data.system.skills || {}; // sicherstellen, dass skills existiert
    for ( let skill of Object.values(out.data.system.skills) ) {
      let attrKey = skill.attribute;
      let attr = out.data.system.attributes[attrKey]; // undefined falls nicht existent
      let extra = (attrKey === "SIN" || attrKey === "WIS" || attrKey === "WIL") ? " (I)" : " (C)";
      let value = attr ? attr.value : 0;
      skill.attrValue = value;
      skill.attrLabel = attr ? attr.label : "unbekanntes Attribut";
      skill.summe = Number(skill.value) + Number(value);
      skill.label += extra;
    }

    // TODO: gehört teilweise in den Jaeger!
    //Kampfskills aufbereiten
    out.data.system.combat = out.data.system.combat || {};
    for ( let skill of Object.values(out.data.system.combat) ) {
      let attrKey = skill.attribute;
      let attr = out.data.system.attributes[attrKey]; // undefined falls nicht existent
      let extra = (attrKey === "SIN" || attrKey === "WIS" || attrKey === "WIL") ? " (I)" : " (C)";
      let value = attr ? attr.value : 0;
      skill.attrValue = value;
      skill.attrLabel = attr ? attr.label : "unbekanntes Attribut";
      skill.summe = Number(skill.value) + Number(value);
      skill.label += extra;
    }
    // TODO: Waffenfertigkeiten ggf. nach equipped sortieren

    // TODO: data.items filtern, sobald alle anderen subtypen abgehandelt
    // out.actor.powers = out.actor.items.filter(i => { return "power" === i.type; });
    // FIXME: 0.8.x
    // out.actor.items = out.actor.items.filter(i => { return "item" === i.type; });

    out.data.system.calc.ldmg_max = Math.min(out.data.system.calc.ap, 5);

    const notes = out.data.system.notes;
    notes.biography.editor = await this._enrichHTML(notes.biography.editor);
    notes.states.editor = await this._enrichHTML(notes.states.editor);
    notes.skills.editor = await this._enrichHTML(notes.skills.editor);
    notes.combat.editor = await this._enrichHTML(notes.combat.editor);
    notes.powers.editor = await this._enrichHTML(notes.powers.editor);
    notes.items.editor = await this._enrichHTML(notes.items.editor);

    out.data = out.data.system; // FIXME Kompatibilität für alte Templates

    return out;
  }

  // TODO: gehört in den Jaeger
  getSkillRolls(key) {
    let actorData = this.actor.system;
    let skill = actorData.skills[key] || actorData.combat[key];
    let attr = actorData.attributes[skill.attribute]; // undefined falls nicht existent
    let value = attr ? attr.value : 0;
    let rolls = Number(skill.value) + Number(value);
    return rolls;
  }

  /* -------------------------------------------- */

  /** @override */
  async _renderInner(data, options={}) {
    let html = await super._renderInner(data, options);

    // TODO: ist _renderInner() oder _replaceHTML() besser?? Sonst Problem: Zugang zu html beim ersten Öffnen
    // Aktualisiere Zustände, die keine Form-Elemente sind
    // oder in activateListener(), foundry macht das auch
    this._updateState(html.find(".eh .controls")[0], "eh", options);
    this._updateState(html.find(".mh .controls")[0], "mh", options);
    this._updateState(html.find(".odmg .controls")[0], "odmg", options);
    this._updateState(html.find(".idmg .controls")[0], "idmg", options);
    this._updateState(html.find(".mdmg .controls")[0], "mdmg", options);
    this._updateState(html.find(".ldmg .controls")[0], "ldmg", options);

    return html;
  }

  _updateState(el, key, options={}) {
    // TODO: geht so nur für resources
    // TODO: auf korrelierendes INPUT type=hidden umstellen
    const curent = this.actor.system.resources[key];
    const max = el.childElementCount;

    for (let i = 0; i < max; i++) {
      el.children[i].dataset.action = i < curent ? "decrease" : "increase";
      let cl = el.children[i].children[0].classList;
      if (i < curent) {
        cl.remove("fa-inverse"); // wird schwarz
      } else {
        cl.add("fa-inverse"); // wird weiss
      }
    }
  }

  /* -------------------------------------------- */

  /** @override */
  activateListeners(html) {
    super.activateListeners(html);

    // Add roll listener
    // TODO: permissions??
    if (game.user.isGM || this.actor.isOwner) {
      html.find(".skills .attributes").on("click", ".roll", this._onClickRoll.bind(this));
      html.find(".skills").on("click", ".li-control", this._onClickRoll.bind(this));
      html.find(".combat").on("click", ".li-control", this._onClickRoll.bind(this));
    }

    // Update Inventory Item
    html.find('.item-edit').click(ev => {
      ev.preventDefault();
      ev.stopPropagation();
      // TODO: Überprüfungen
      const li = $(ev.currentTarget).parents(".item");
      const item = this.actor.items.get(li.data("itemId"));
      const app = item.sheet.render(true);
      HexxenAppAlignmentHelper.align(app, ev);
    });

    // Toggle description
    html.find('.items-list .item h4').on("click", ev => {
      // TODO: Zielelement sicherer identifizieren
      $(ev.currentTarget.nextElementSibling).toggle();
    });

    // Everything below here is only needed if the sheet is editable
    if (!this.options.editable) return;

    // Create OwnedItem
    html.find('.item-create').click(this._onItemCreate.bind(this));

    // Delete Inventory Item
    // TODO: Listener von class item-delete auf data-action delete umstellen, auch .html
    html.find('.item-delete').click(ev => {
      ev.preventDefault();
      ev.stopPropagation();
      // TODO: Überprüfungen
      const li = $(ev.currentTarget).parents(".item");
      this.actor.items.get(li.data("itemId"))?.delete();
      li.slideUp(200, () => this.render(false));
    });

    // Item controls bei hover anzeigen
    // TODO: Hover Helper verallgemeinern
    //html.find(".item").hover(HexxenIncDecHelper.onHoverPlusMinus);
    html.find("a[data-action='select']").on("click", HexxenCompendiumHelper.onClickOpenCompendium);

    // +/- Buttons
    // Segnungen, Ideen, Coups
    html.find(".sheet-header .inc-btn").hover(HexxenIncDecHelper.onHoverPlusMinus.bind(this));
    html.find(".sheet-header .inc-btn").on("click", ".control", HexxenIncDecHelper.onClickPlusMinus.bind(this));
    html.find(".sheet-header .set-default").on("click", HexxenIncDecHelper.onClickPlusMinus.bind(this));
    // Erste Hilfe, Mag. Heilung, Elixire
    html.find(".states .res-boxes").on("click", ".control", HexxenIncDecHelper.onClickPlusMinus.bind(this));
    html.find('.items .consumable-controls').on('click', this._incDecItemQuantity.bind(this));
  }

  /* -------------------------------------------- */

  async _incDecItemQuantity(event) {
    event.preventDefault();
    event.stopPropagation();
    const targetItem = this.actor.items.get(event.target.closest('.item')?.dataset.itemId);
    const action = event.target.closest('.control')?.dataset.action;
    await targetItem?.update({'system.quantity': Math.max(targetItem.system.quantity + (action === 'increase' ? 1 : -1), 0)});
  }

  // TODO: async notwendig?
  async _onItemCreate(event) {
    console.log(event);
    // TODO: unterschiedliche Typen auswählbar machen
    this.actor.createOwnedItem({name: "New Item", type: "item"}, {renderSheet: true});
  }

  async _onClickRoll(event) {
    event.preventDefault();
    const a = event.currentTarget;
    const action = a.dataset.action;

    const type = a.dataset.type;
    const key = a.parentNode.dataset.key;

    const form = this.form;

    // console.log(event);

    // shift or ctrl click --> delegate
    if ( event.originalEvent.shiftKey || event.originalEvent.ctrlKey ) {
      new HexxenRoller(this.actor, /* options */ {
        top: this.position.top + 40,
        left: this.position.left + ((this.position.width - 400) / 2)
      }, /* hints */ {
        type: type,
        key: key
      }).render(true);
      return;
    }

    this._handleRoll(action, type, key);
  }

  _handleRoll(action, type, key) {
    const actorData = this.actor.system;
    const attrs = actorData.attributes;
    let rolls = 0;
    let label = "";
    if ( action === "roll" && type === "attribute" ) {
      rolls = attrs[key].value;
      label = attrs[key].label;
    }
    else {
      rolls = this.getSkillRolls(key);
      let target = actorData.skills[key] || actorData.combat[key];
      label = target.label;
      if (target.schaden) label += ` (SCH +${target.schaden})`;
    }

    HexxenRollHelper.rollToChat(this.actor, { h: rolls }, label);
  }

  _onDragStart(event) {
    // console.log('jaeger _onDragStart', event)
    if (event.target.classList.contains('roll'))
    {
      // Create drag data for an owned item
      const a = event.currentTarget;
      const action = a.dataset.action;

      const type = a.dataset.type;
      const key = a.parentNode.dataset.key;

      const dragData = {
        type: "HexxenRoll",
        actorId: this.actor.id,
        data: {type: type, key: key}
      };
      if (this.actor.isToken) {
        dragData.sceneId = canvas.scene.id;
        dragData.tokenId = this.actor.token.id;
      }

      // Set data transfer
      event.dataTransfer.setData("text/plain", JSON.stringify(dragData));
      return;
    }
    return super._onDragStart(event);
  }

  // TODO: _can* beeinflusst nur die Registrierung der Listener, nicht den Vorgang an sich!
  // /** @override */
  // _canDragStart(selector) {
  //   console.log('jaeger _canDragStart', selector)
  //   return super._canDragStart(selector);
  // }
  
  // /** @override */
  // _canDragDrop(selector) {
  //   console.log('jaeger _canDragDrop', selector)
  //   return super._canDragDrop(selector);
  // }
  
  // /** @override */
  // _onDragOver(event) {
  //   console.log('jaeger _onDragOver', event)
  //   return super._onDragOver(event);
  // }
  
  // /** @override */
  // _onDrop(event) {
  //   console.log('jaeger _onDrop', event)
  //   return super._onDrop(event);
  // }
  
  /* -------------------------------------------- */

  /** @override */
  async _updateObject(event, formData) {
    // redirect embedded item updates
    const itemKeys = Object.keys(formData).filter(key => key.startsWith('hx:item.'));
    for (const itemKey of itemKeys) {
      const value = formData[itemKey]; // TODO: NaN??
      delete formData[itemKey];
      const [, itemId, ...propertyPath] = itemKey.split('.');
      const property = propertyPath.join('.');
      const item = this.actor.items.get(itemId);
      const currentValue = getProperty(item, property);
      if (value !== currentValue) {
        await item.update({[property]: value});
      }
    }

    // TODO: ungültige Eingaben in numerischen Textfeldern filtern (NaN oder null)
    for (const key in formData) {
      if (formData.hasOwnProperty(key)) {
        const data = formData[key];
        // if (data === null) formData[key] = 0; // TODO: Defaultwert?
        // else
        if (typeof(data) === "number" && isNaN(data)) {
          ui.notifications.warn(`Ungültige Eingabe für ${key}`); // TODO: was machen?
          // delete formData[key];
        }
      }
    }

    // TODO: temporary code until data structure change in actor
    if (formData.hasOwnProperty("data.languages.value")){
      formData["data.languages.0.value"] = formData["data.languages.value"];
      delete formData["data.languages.value"];
    }

    // Update the Actor
    return await super._updateObject(event, formData);
  }
}
