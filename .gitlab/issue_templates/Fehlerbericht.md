**Womit arbeitest du?**
 - OS: [z.B. Windows, MacOS, Linux, NodeJS] [Version]
 - Browser: [z.B. Electron, Chrome, Safari, Firefox, Edge] [Version]
 - Foundry-Version: [z.B. 10.288]
 - HeXXen-Version: [z.B. 1.1.0]
 - weitere Module: *Bitte prüfe, ob der Fehler auch auftritt, wenn keine weiteren Module aktiviert sind.*

**Beschreibung des Fehlers. / Wie kann der Fehler reproduziert werden?**
*Beschreibe den Fehler sowie die zur Reproduktion notwendigen Schritte möglichst klar. Falls sinnvoll, füge bitte einen Screenshot hinzu. Wenn etwas falsch angezeigt wird, bitte erkläre, was du erwarten würdest.*

**Zusätzliche Informationen**
*Falls möglich: Bitte prüfe, ob Fehlermeldungen auf der Entwicklerkonsole vorhanden sind (wird in den meisten Browsern mit F12 aufgerufen).*
